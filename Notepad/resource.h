//{{NO_DEPENDENCIES}}
// Включаемый файл, созданный в Microsoft Visual C++.
// Используется Notepad.rc
//
#define IDC_MYICON                      2
#define IDD_NOTEPAD_DIALOG              102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_NOTEPAD                     107
#define IDI_SMALL                       108
#define IDC_NOTEPAD                     109
#define IDR_MAINFRAME                   128
#define IDD_REPLACE                     129
#define IDC_REPLACE_TO                  1000
#define IDC_REPLACE_FROM                1001
#define REPLACE_IDTHIS                  1002
#define IDALL2                          1003 
#define REPLACE_IDALL                   1003
#define REPLACE_IDNEXT                  1004
#define REPLACE_IDCANCEL                1005
#define ID_EDIT_UNDO                    32771
#define ID_EDIT_CUT                     32772
#define ID_EDIT_COPY                    32773
#define ID_EDIT_PASTE                   32774
#define ID_EDIT_SELECTALL               32775
#define ID_32776                        32776
#define ID_32777                        32777
#define ID_OPEN                         32778
#define ID_SAVE                         32779
#define ID_32780                        32780
#define ID_SAVE_AS                      32781
#define ID_EDIT_REPLACE                 32782
#define ID_EDIT_FIND                    32783
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32784
#define _APS_NEXT_CONTROL_VALUE         1006
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
