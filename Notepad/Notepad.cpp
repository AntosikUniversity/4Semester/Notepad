// Notepad.cpp: ���������� ����� ����� ��� ����������.
//

#include "stdafx.h"
#include "Notepad.h"

#define MAX_LOADSTRING 100

// ���������� ����������:
HINSTANCE hInst;                                // ������� ���������
WCHAR szTitle[MAX_LOADSTRING];                  // ����� ������ ���������
WCHAR szWindowClass[MAX_LOADSTRING];            // ��� ������ �������� ����

// ��������� ���������� �������, ���������� � ���� ������ ����:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK	EditProc(HWND wnd, UINT msg, WPARAM wParam, LPARAM lParam);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    Replace(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
					  _In_opt_ HINSTANCE hPrevInstance,
					  _In_ LPWSTR    lpCmdLine,
					  _In_ int       nCmdShow) {
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO: ���������� ��� �����.

	// ������������� ���������� �����
	LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadStringW(hInstance, IDC_NOTEPAD, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// ��������� ������������� ����������:
	if (!InitInstance(hInstance, nCmdShow)) {
		return FALSE;
	}

	HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_NOTEPAD));

	MSG msg;

	// ���� ��������� ���������:
	while (GetMessage(&msg, nullptr, 0, 0)) {
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int)msg.wParam;
}



//
//  �������: MyRegisterClass()
//
//  ����������: ������������ ����� ����.
//
ATOM MyRegisterClass(HINSTANCE hInstance) {
	WNDCLASSEXW wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_NOTEPAD));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_NOTEPAD);
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassExW(&wcex);
}

//
//   �������: InitInstance(HINSTANCE, int)
//
//   ����������: ��������� ��������� ���������� � ������� ������� ����.
//
//   �����������:
//
//        � ������ ������� ���������� ���������� ����������� � ���������� ����������, � �����
//        ��������� � ��������� �� ����� ������� ���� ���������.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow) {
	hInst = hInstance; // ��������� ���������� ���������� � ���������� ����������

	HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
							  CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

	if (!hWnd) {
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	return TRUE;
}

//
//  �������: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  ����������:  ������������ ��������� � ������� ����.
//
//  WM_COMMAND � ���������� ���� ����������
//  WM_PAINT � ���������� ������� ����
//  WM_DESTROY � ��������� ��������� � ������ � ���������
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
	TCHAR str[256] = "";
	DWORD dwBytesWritten = 0;
	DWORD dwBytesRead = 0;
	switch (message) {
		case WM_CREATE:
		{
			LoadLibrary(TEXT("Msftedit.dll"));
			EditControl = CreateWindowEx(
				0,
				"RICHEDIT50W", //(LPCSTR)MSFTEDIT_CLASS,				// predefined class 
				NULL,					// no window title 
				WS_CHILD | WS_VISIBLE | WS_VSCROLL |
				ES_LEFT | ES_MULTILINE | ES_AUTOVSCROLL,
				0, 0, 0, 0,				// set size in WM_SIZE message 
				hWnd,					// parent window 
				(HMENU)ID_EDITCHILD,	// edit control ID 
				(HINSTANCE)GetWindowLong(hWnd, -6),
				NULL);					// pointer not needed 
			EditProcFunc = (WNDPROC)SetWindowLongPtr(EditControl, -4, (LONG_PTR)&EditProc);

			EditTextFormat.cbSize = sizeof(CHARFORMAT);
			EditTextFormat.dwMask = CFM_COLOR | CFM_SIZE;	// | CFM_BOLD
			EditTextFormat.dwEffects = NULL;	//CFE_BOLD
			EditTextFormat.crTextColor = RGB(0, 0, 0);
			EditTextFormat.yHeight = 300;

			EditTextFormatted = EditTextFormat;
			EditTextFormatted.crTextColor = RGB(86, 156, 214);
			EditTextCommented = EditTextFormat;
			EditTextCommented.crTextColor = RGB(77, 144, 58);

			SendMessage(EditControl, EM_SETCHARFORMAT, SCF_ALL, (LPARAM)&EditTextFormat);
			break;
		}
		case WM_COMMAND:
		{
			int wmId = LOWORD(wParam);
			// ��������� ����� � ����:
			switch (wmId) {
				case ID_EDIT_REPLACE:
					if (!IsWindow(ReplaceDialog)) {
						ReplaceDialog = CreateDialog(hInst,
													 MAKEINTRESOURCE(IDD_REPLACE),
													 hWnd,
													 (DLGPROC)Replace);
						ShowWindow(ReplaceDialog, SW_SHOW);
					}
					break;
				case IDM_ABOUT:
					DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
					break;
				case IDM_EXIT:
					DestroyWindow(hWnd);
					break;
				case ID_EDIT_UNDO:
					if (SendMessage(EditControl, EM_CANUNDO, 0, 0))
						SendMessage(EditControl, WM_UNDO, 0, 0);
					break;
				case ID_EDIT_CUT:
					SendMessage(EditControl, WM_CUT, 0, 0);
					break;
				case ID_EDIT_COPY:
					SendMessage(EditControl, WM_COPY, 0, 0);
					break;
				case ID_EDIT_PASTE:
					SendMessage(EditControl, WM_PASTE, 0, 0);
					break;
				case ID_EDIT_SELECTALL:
					SendMessage(EditControl, EM_SETSEL, 0, -1);
					break;
				case ID_OPEN:
					ZeroMemory(&file, sizeof(OPENFILENAME));
					file.lStructSize = sizeof(file);
					file.hwndOwner = hWnd;
					file.lpstrFile = str;
					file.nMaxFile = sizeof(str);
					file.lpstrFilter = _T("Text Format (*.txt)\0*.txt");
					if (GetOpenFileName(&file) == TRUE) {
						dwBytesRead = 0;
						hFile = CreateFile(file.lpstrFile, GENERIC_READ,
										   0, (LPSECURITY_ATTRIBUTES)NULL,
										   OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL,
										   (HANDLE)NULL);
						ReadFile(hFile, &EditText, sizeof(EditText) - 1, &dwBytesRead, NULL);
						CloseHandle(hFile);
						EditText[dwBytesRead] = '\0';
						SetWindowText(EditControl, EditText);
					} else MessageBox(hWnd, _T("Can't open file dialog, lol"), _T("ERROR!"), MB_OK);
					break;
				case ID_SAVE:
					ZeroMemory(&file, sizeof(OPENFILENAME));
					dwBytesWritten = 0;
					file.lStructSize = sizeof(file);
					file.hwndOwner = hWnd;
					file.lpstrFile = str;
					file.nMaxFile = sizeof(str);
					file.lpstrFilter = _T("Text Format (*.txt)\0*.txt");
					file.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;
					if (GetSaveFileName(&file) == TRUE)
						hFile = CreateFile(file.lpstrFile, GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, (LPSECURITY_ATTRIBUTES)NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, (HANDLE)NULL);
					GetWindowText(EditControl, EditText, sizeof(EditText));
					WriteFile(hFile, EditText, sizeof(EditText), &dwBytesWritten, NULL);
					CloseHandle(hFile);
					break;
				default:
					return DefWindowProc(hWnd, message, wParam, lParam);
			}
		}
		break;
		case WM_SETFOCUS:
			SetFocus(EditControl);
			return 0;
			break;
		case WM_SIZE:
			MoveWindow(EditControl,
					   0, 0,					// starting x- and y-coordinates 
					   LOWORD(lParam),			// width of client area 
					   HIWORD(lParam),			// height of client area 
					   TRUE);					// repaint window 
			return 0;
			break;
		case WM_PAINT:
		{
			PAINTSTRUCT ps;
			HDC hdc = BeginPaint(hWnd, &ps);
			// TODO: �������� ���� ����� ��� ����������, ������������ HDC...
			EndPaint(hWnd, &ps);
		}
		break;
		case WM_DESTROY:
			PostQuitMessage(0);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// ���������� ��������� ��� ���� "� ���������".
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam) {
	UNREFERENCED_PARAMETER(lParam);
	switch (message) {
		case WM_INITDIALOG:
			return (INT_PTR)TRUE;

		case WM_COMMAND:
			if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL) {
				EndDialog(hDlg, LOWORD(wParam));
				return (INT_PTR)TRUE;
			}
			break;
	}
	return (INT_PTR)FALSE;
}

