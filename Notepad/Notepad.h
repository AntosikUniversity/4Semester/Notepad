#pragma once
#include "resource.h"
#include "Richedit.h"
#include "Commdlg.h"
#include <string>
#include <vector>
#include <algorithm>
#include <iterator>
#include <locale>
#include <ctype.h>

/* DEFINE */
#define ID_EDITCHILD 100
#define MAX_TAG_SIZE 10
#define SoloLine 1
#define MultiLine 2
/* DEFINE */


/* STRUCTURES */
// word struct
struct word {
	int begin;
	int end;
	std::string str;
	word(int b, int e, std::string s) {
		begin = b;
		end = e;
		str = s;
	}
};

// comment struct
struct comment {
	int begin;
	int end;
	bool type;								// true - multiline, false - oneline
	comment(int b, int e, bool t) {
		begin = b;
		end = e;
		type = t;
	}
	comment(int b, bool t) {
		begin = b;
		end = -1;
		type = t;
	}
};
/* STRUCTURES */


/* VARS */
// technical
WNDPROC EditProcFunc = NULL;
HWND EditControl = NULL, ReplaceDialog = NULL, FindDialog = NULL;
OPENFILENAME file = { 0 };
HANDLE hFile = NULL;

// text related
std::vector<word> words;
int textlength_global = 0;
TCHAR * EditText = NULL;
CHARFORMAT EditTextFormat = { 0 }, EditTextFormatted = { 0 }, EditTextCommented = { 0 };

// Comment related
int IsComStart = 0;
int LocComStart;

// replace & find vars
std::vector<word> searchWords;
std::string wordFrom, wordTo;
std::vector<word>::iterator it;
int n = 0; bool repl = false;
/* VARS */


/* FUNC DECLARATION */
// formatting
bool EditCheckWord(word w);
void EditFormatWords();

// replace & find
std::vector<word> findString(std::string str, std::string substr);
void selectWord(word & w);
void replaceString(word & w, TCHAR * str);
void recalcAfterReplace(std::vector<word> & list, int length);
TCHAR * StrToTCHAR(std::string & str);
/* FUNC DECLARATION */



/* FUNC REALIZATION */
// edit process control
LRESULT CALLBACK EditProc(HWND wnd, UINT msg, WPARAM wParam, LPARAM lParam) {
	switch (msg) {
		case WM_COPY: case WM_PASTE: case WM_CUT: case WM_KEYUP: case WM_UNDO: case WM_CLEAR:
		{
			int textlength_local = GetWindowTextLength(EditControl);
			if (textlength_local != textlength_global)
				EditFormatWords();
		}
		default:
			return CallWindowProc(EditProcFunc, wnd, msg, wParam, lParam);
	}
	return 0;
}

// is word = tag
bool EditCheckWord(word w) {
	std::vector<std::string> strings = { "for", "do", "while", "int", "string", "if", "else", "bool", "void", "switch", "case", "int", "default", "break", "return" };
	for (auto &str : strings) {
		if (str.compare(w.str) == 0)
			return true;
	}
	return false;
}

// format our tag
void EditFormatWords() {
	std::locale loc2("");
	// default init vars
	int textlength_local = 0;
	std::string strEditText = "";
	DWORD selStart = 0, selEnd = 0;
	words.clear();

	// init text vars
	textlength_local = GetWindowTextLength(EditControl);
	delete EditText;
	EditText = new TCHAR[textlength_local + 1];
	GetWindowText(EditControl, EditText, textlength_local + 1);
	strEditText = EditText;
	strEditText.erase(std::remove(strEditText.begin(), strEditText.end(), '\r'), strEditText.end());

	// init int vars
	int textlength_diff = textlength_local - textlength_global;
	SendMessage(EditControl, EM_GETSEL, (WPARAM)&selStart, (LPARAM)&selEnd);

	// lets parse keywords
	int pos = -1;
	for (int i = 0, len = strEditText.length(); i < len + 1; i++) {
		if (!std::isalnum(strEditText[i], loc2) || strEditText[i] == '\n') {
			std::string w = strEditText.substr(pos + 1, i - pos - 1);
			if (w != "" && (pos + 1 != i))
				words.push_back(word(pos + 1, i, w));
			pos = i;
		}
	}

	SendMessage(EditControl, EM_HIDESELECTION, TRUE, NULL);
	// apply words selection
	for (auto &word : words) {
		SendMessage(EditControl, EM_SETSEL, word.begin, word.end);
		if (EditCheckWord(word))
			SendMessage(EditControl, EM_SETCHARFORMAT, SCF_SELECTION, (LPARAM)&EditTextFormatted);
		else
			SendMessage(EditControl, EM_SETCHARFORMAT, SCF_SELECTION, (LPARAM)&EditTextFormat);
		SendMessage(EditControl, EM_SETSEL, selStart, selEnd);
	}

	// lets parse comments
	for (int g = 1; g < textlength_local; g++) {
		switch (strEditText[g]) {
			case '/':
				switch (strEditText[g - 1]) {
					case '/':
						if (IsComStart == 0) {
							LocComStart = g - 1;
							IsComStart = SoloLine;
						}
						break;
					case '*':
						if ((IsComStart == MultiLine) && (g > LocComStart)) {
							SendMessage(EditControl, EM_SETSEL, LocComStart, g + 1);
							SendMessage(EditControl, EM_SETCHARFORMAT, SCF_SELECTION, (LPARAM)&EditTextCommented);
							SendMessage(EditControl, EM_SETSEL, selStart, selEnd);
							IsComStart = 0;
						}
						break;
				}
				break;
			case '*':
				switch (strEditText[g - 1]) {
					case '/':
						if (IsComStart == 0) {
							IsComStart = MultiLine;
							LocComStart = g - 1;
						}
						break;
				}
				break;
			case '\n':
				if ((IsComStart == SoloLine) && (g > LocComStart)) {
					SendMessage(EditControl, EM_SETSEL, LocComStart, g + 1);
					SendMessage(EditControl, EM_SETCHARFORMAT, SCF_SELECTION, (LPARAM)&EditTextCommented);
					SendMessage(EditControl, EM_SETSEL, selStart, selEnd);
					IsComStart = 0;
				}
				break;
			default:
				if ((IsComStart == SoloLine) && (g > LocComStart)) {
					SendMessage(EditControl, EM_SETSEL, LocComStart, g + 1);
					SendMessage(EditControl, EM_SETCHARFORMAT, SCF_SELECTION, (LPARAM)&EditTextCommented);
					SendMessage(EditControl, EM_SETSEL, selStart, selEnd);
				}
				if ((IsComStart == MultiLine) && (g > LocComStart)) {
					SendMessage(EditControl, EM_SETSEL, LocComStart, g + 1);
					SendMessage(EditControl, EM_SETCHARFORMAT, SCF_SELECTION, (LPARAM)&EditTextCommented);
					SendMessage(EditControl, EM_SETSEL, selStart, selEnd);
				}
				break;
		}

	}

	textlength_global = textlength_local;
}


// ���������� ��������� ��� ���� "Replace".
INT_PTR CALLBACK Replace(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam) {
	UNREFERENCED_PARAMETER(lParam);

	// define vars
	HWND editFrom = NULL, editTo = NULL;
	std::string wordFrom_local, wordTo_local;
	TCHAR buff[1024], *temp = NULL;

	// init vars
	editFrom = GetDlgItem(hDlg, IDC_REPLACE_FROM);
	editTo = GetDlgItem(hDlg, IDC_REPLACE_TO);

	switch (message) {
		case WM_INITDIALOG:
			return (INT_PTR)TRUE;

		case WM_COMMAND:
			if (LOWORD(wParam) == REPLACE_IDCANCEL) {
				// close window if cancel clicked
				EndDialog(hDlg, LOWORD(wParam));
				ReplaceDialog = NULL;
				return (INT_PTR)TRUE;

			} else if (LOWORD(wParam) == REPLACE_IDALL || LOWORD(wParam) == REPLACE_IDNEXT || LOWORD(wParam) == REPLACE_IDTHIS) {
				// handling replace
				GetWindowText(editFrom, buff, 1024);
				wordFrom_local = buff;
				GetWindowText(editTo, buff, 1024);
				wordTo_local = buff;

				// check if first search/another words to find/replace
				if (searchWords.empty() || wordFrom_local != wordFrom || wordTo_local != wordTo) {
					searchWords = findString(EditText, wordFrom_local);
					n = 0;
					wordFrom = wordFrom_local;
					wordTo = wordTo_local;
				}

				// init iterator & iterator helper
				if (n == 0 && searchWords.size() > 0) {
					it = searchWords.begin(); n = searchWords.size();
				} else if (n == 0 && searchWords.size() == 0)
					return (INT_PTR)FALSE;

				// handle next button and highlight next found string
				if (LOWORD(wParam) == REPLACE_IDNEXT) {
					if (!repl) {
						if (std::prev(searchWords.end()) == it) {
							it = searchWords.begin();
							n = searchWords.size();
						} else {
							it++; n--;
						}
					} else
						repl = false;
					selectWord(*it);

					// handle "replace" button
				} else if (LOWORD(wParam) == REPLACE_IDTHIS) {
					temp = StrToTCHAR(wordTo);
					replaceString(*it, temp);
					int len = wordTo.length() - wordFrom.length();
					it = searchWords.erase(it);
					repl = true;
					n--;
					recalcAfterReplace(searchWords, len);
					delete temp;

					// handle "replace all" button
				} else if (LOWORD(wParam) == REPLACE_IDALL) {
					temp = StrToTCHAR(wordTo);
					for (std::vector<word>::reverse_iterator rit = searchWords.rbegin(); rit != searchWords.rend(); ++rit) {
						replaceString(*rit, temp);
					}
					searchWords.clear();
					n = 0;
					delete temp;
				}
			}
			break;
	}
	return (INT_PTR)FALSE;
}

// find substring in string, return word vector
std::vector<word> findString(std::string str, std::string substr) {
	std::vector<word> result;
	std::string strEditText = EditText;

	size_t pos = str.find(substr, 0);
	while (pos != std::string::npos) {
		result.push_back(word(pos, pos + substr.length(), substr));
		pos = str.find(substr, pos + 1);
	}

	return result;
}

// highlight finded word
void selectWord(word & w) {
	SendMessage(EditControl, EM_SETSEL, w.begin, w.end);
	SendMessage(EditControl, EM_HIDESELECTION, NULL, NULL);
	return;
}

// replace string in text
void replaceString(word & w, TCHAR * str) {
	SendMessage(EditControl, EM_SETSEL, w.begin, w.end);
	SendMessage(EditControl, EM_REPLACESEL, 0, (LPARAM)str);
	EditFormatWords();
}

// recalculate postions after replacing
void recalcAfterReplace(std::vector<word> & list, int length) {
	for (auto &w : list) {
		w.begin += length;
		w.end += length;
	};
}

// convert std::string to TCHAR
TCHAR * StrToTCHAR(std::string & str) {
	TCHAR *param = new TCHAR[str.size() + 1];
	param[str.size()] = 0;
	std::copy(str.begin(), str.end(), param);
	return param;
}